NAMESPACE		=	serializable-type.docker.dvlpm.ru
APP_ROOT		=	/app
WORKING_DIR		=	$(CURDIR)

export USER_ID ?= $(shell id -u)
export GROUP_ID ?= $(shell id -g)
export DOCKER_BUILDKIT := 1

define docker-run
	docker run --rm \
		-e GOSU=true \
		-e USER_ID=$(USER_ID) \
		-e GROUP_ID=$(GROUP_ID) \
		$1 \
		-v $(CURDIR):$(APP_ROOT) \
		$(NAMESPACE)/php:latest \
		$2
endef

build: #? Build dev container image
	$(call about,Build PHP image)
	docker build \
		-t $(NAMESPACE)/php:latest \
		-f docker/Dockerfile \
		docker

install: #? Run composer install
	$(call docker-run, \
		-v $(abspath $(HOME)/.ssh):/home/www-data/.ssh \
		-v $(abspath $(HOME)/.composer):/var/composer, \
		composer install \
	)

shell: #? Open dev container shell
	$(call docker-run, \
		-it \
		-v $(abspath $(HOME)/.ssh):/home/www-data/.ssh \
		-v $(abspath $(HOME)/.composer):/var/composer, \
		bash \
	)
