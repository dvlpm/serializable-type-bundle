# Dvlpm Serializable bundle

## Installation

Usage is as simple as 

```bash
composer require dvlpm/serializable-type-bundle:^1.0
```

```php
// Kernel

public function registerBundles()
{
    // ...
    $bundles[] = new \Dvlpm\SerializableTypeBundle\DvlpmSerializableTypeBundle();
    // ...
}
```

```yaml
# config.yml
dvlpm_serializable_type:
    dbal_types:
        my_domain_type: My\Domain\Type
```

```php
class MyEntity
{
    /** @ORM\Column(type="my_domain_type") */
    private $value;
}
```

This will enable conversion of value field from your type
