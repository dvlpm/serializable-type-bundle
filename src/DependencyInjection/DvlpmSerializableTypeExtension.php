<?php

declare(strict_types=1);

namespace Dvlpm\SerializableTypeBundle\DependencyInjection;

use Dvlpm\SerializableTypeBundle\Doctrine\DBAL\Type\SerializableTypeInitializer;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class DvlpmSerializableTypeExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configs = $this->processConfiguration(new Configuration(), $configs);

        $typeInitializer = $container->register(SerializableTypeInitializer::class, SerializableTypeInitializer::class);
        $typeInitializer->setPublic(true);
        $typeInitializer->setAutowired(true);

        foreach ($configs['dbal_types'] ?? [] as $typeName => $serializableClass) {
            $typeInitializer->addMethodCall('initialize', [$typeName, $serializableClass]);
        }
    }
}
