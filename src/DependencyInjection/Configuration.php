<?php

namespace Dvlpm\SerializableTypeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    /** {@inheritdoc} */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $name = 'dvlpm_serializable_type';
        $builder = new TreeBuilder($name);
        $root = $builder->getRootNode();

        $root->children()
                ->arrayNode('dbal_types')
                ->scalarPrototype()
             ->end();

        return $builder;
    }
}
