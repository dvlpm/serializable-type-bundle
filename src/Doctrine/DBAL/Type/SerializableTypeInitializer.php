<?php

declare(strict_types=1);

namespace Dvlpm\SerializableTypeBundle\Doctrine\DBAL\Type;

use Doctrine\DBAL\Types\Type;
use Symfony\Component\Serializer\SerializerInterface;

final class SerializableTypeInitializer
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function initialize(string $typeName, string $serializableClass): void
    {
        $typeRegistry = Type::getTypeRegistry();

        $type = new SerializableType();
        $type->initialize($this->serializer, $serializableClass);

        if ($typeRegistry->has($typeName)) {
            $typeRegistry->override($typeName, $type);
        } else {
            $typeRegistry->register($typeName, $type);
        }
    }
}
