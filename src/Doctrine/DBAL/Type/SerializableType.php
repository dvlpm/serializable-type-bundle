<?php

declare(strict_types=1);

namespace Dvlpm\SerializableTypeBundle\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final class SerializableType extends JsonType
{
    private SerializerInterface $serializer;

    private string $serializableType;

    public function initialize(SerializerInterface $serializer, string $serializableType)
    {
        $this->serializer = $serializer;
        $this->serializableType = $serializableType;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        if ($value === null) {
            return null;
        }

        return $this->serializer->deserialize($value, $this->serializableType, JsonEncoder::FORMAT);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        return $this->serializer->serialize(
            $value,
            JsonEncoder::FORMAT,
            [
                AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
            ]
        );
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return false;
    }
}
