<?php

declare(strict_types=1);

namespace Dvlpm\SerializableTypeBundle;

use Dvlpm\SerializableTypeBundle\Doctrine\DBAL\Type\SerializableTypeInitializer;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DvlpmSerializableTypeBundle extends Bundle
{
    public function boot(): void
    {
        // Fetch service from the container in order to register doctrine types
        $this->container->get(SerializableTypeInitializer::class);
    }
}
