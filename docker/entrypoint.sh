#!/bin/bash
set -e

. /etc/profile

loadfrom() {
  if [ -d "${1}" ]; then
    if [ "$(ls ${1})" ]; then
      for f in ${1}/*.sh; do
        . "$f"
      done
    fi
  fi
}

if [ "$(id -u)" -eq "0" ]; then
  loadfrom ${ENTRYPOINT_DIR}
  set +e
  case "${GOSU}" in
    "true")
      exec su-exec ${GOSU_USER} "$@"
    ;;
    *)
      exec "$@"
    ;;
  esac
fi

set +e
exec "$@"
