#!/bin/bash

DEFAULT_UID=`id -u ${USER_NAME}`
DEFAULT_GID=`id -g ${USER_NAME}`

_chgrp() {
  group_id="$1"
  dir="$2"

  if [ "$(stat -c "%g" "$dir")" != "$group_id" ]; then
    chgrp -R ${group_id} $dir
  fi
}

_chown() {
  user_id="$1"
  dir="$2"

  if [ "$(stat -c "%u" "$dir")" != "$user_id" ]; then
    chown -R "$user_id" "$dir"
  fi
}

if [ "${GROUP_ID}" != "${DEFAULT_GID}" ]; then
  groupmod -o -g ${GROUP_ID} ${GROUP_NAME}
  _chgrp ${GROUP_ID} /home/${USER_NAME}
  _chgrp ${GROUP_ID} ${COMPOSER_HOME}
  _chgrp ${GROUP_ID} ${WORK_DIR}
fi

if [ "${USER_ID}" != "${DEFAULT_UID}" ]; then
  usermod -o -u ${USER_ID} ${USER_NAME}
  _chown ${USER_ID} /home/${USER_NAME}
  _chown ${USER_ID} ${COMPOSER_HOME}
  _chown ${USER_ID} ${WORK_DIR}
fi
