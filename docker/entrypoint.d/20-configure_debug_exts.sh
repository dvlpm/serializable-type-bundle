#!/bin/bash

if [ "${XDEBUG_ENABLE}" != "true" ]; then
  rm -f ${PHP_INI_DIR}/conf.d/xdebug.ini
fi
