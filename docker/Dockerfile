FROM php:8.0.7-fpm-alpine3.12

# Install php extension
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN apk add --no-cache --update git openssh-client openssh-keygen bash bash-completion su-exec shadow \
    && install-php-extensions \
        zip \
        xdebug \
        @composer

ENV USER_NAME=www-data \
    GROUP_NAME=www-data \
    USER_ID=82 \
    GROUP_ID=82 \
    GOSU=false \
    GOSU_USER=www-data:www-data \
    ENTRYPOINT_DIR=/etc/entrypoint.d \
    WORK_DIR=/app \
    COMPOSER_HOME=/var/composer \
    COMPOSER_MEMORY_LIMIT=-1 \
    XDEBUG_ENABLE=true

# Ensure user exists
RUN set -eux; \
    addgroup -g ${GROUP_ID} ${GROUP_NAME} \
    && adduser -G ${GROUP_NAME} -u ${USER_ID} -s "/bin/bash" -D ${USER_NAME}; \
    set +eux; \
    mkdir -p ${WORK_DIR} \
    && chown ${USER_NAME}:${GROUP_NAME} -R ${WORK_DIR} \
    && mkdir /home/${USER_NAME}/.ssh \
    && echo -e "Host stash.dvlpm.ru\n\tIdentityFile ~/.ssh/id_rsa\n" > /home/${USER_NAME}/.ssh/config \
    && echo -e "Host stash.dvlpm.ru\n\tStrictHostKeyChecking no\n"   >> /home/${USER_NAME}/.ssh/config \
    && chown ${USER_NAME}:${GROUP_NAME} -R /home/${USER_NAME}/.ssh \
    && mkdir -p ${COMPOSER_HOME} \
    && chown ${USER_NAME}:${GROUP_NAME} -R ${COMPOSER_HOME}

# Add entrypoint scripts
COPY entrypoint.d/ ${ENTRYPOINT_DIR}/
COPY entrypoint.sh /entrypoint.sh

WORKDIR ${WORK_DIR}
ENTRYPOINT ["/entrypoint.sh"]
CMD ["php-fpm"]
